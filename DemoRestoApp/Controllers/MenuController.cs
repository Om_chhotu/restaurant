﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoRestoApp.Controllers
{
    [Authorize]
    public class MenuController : Controller
    {
        // GET: Menu
        private RestaurantEntities1 db = new RestaurantEntities1();

        // GET: Employees
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DisplayVeg()
        {
            return View(db.Veg.ToList());
        }

        public ActionResult DisplayNonVeg()
        {
            return View(db.NonVeg.ToList());
        }
        public ActionResult DisplayDrinks()
        {
            return View(db.Drinks.ToList());
        }

    }
}