﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoRestoApp.Models
{
    public class Membership
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="please enter your user name")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "please enter your password")]
        public string Password { get; set; }
    }
}